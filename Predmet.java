/**
 * Trida predmet, umistitelna do mistnosti, sebratelna/nesebratelna
 * Pri pridani do mistnosti je potreba implementovat jeji funkcnost pri
 * vytvareni scenare adventury
 * Created by matejcermak on 14.03.18.
 */
public abstract class Predmet {

    private String nazev;
    private float vaha;
    private boolean nositelne;

    /**
     * @param nazev
     * @param vaha      vaha predmetu pro vlozeni do batohu
     * @param nositelne zda-li je sebratelna, vlozitelna do batohu
     */
    public Predmet( String nazev, float vaha, boolean nositelne ) {
        this.nazev = nazev;
        this.vaha = vaha;
        this.nositelne = nositelne;
    }

    /**
     * @param postava postava, na kterou lze predmet pouzit
     * @return bool zda byla vec pouzita spravne
     */
    public abstract boolean pouzit( Postava postava );

    @Override
    public boolean equals( Object o ) {
        if ( this == o ) return true;
        if ( o == null || getClass() != o.getClass() ) return false;

        Predmet predmet = (Predmet) o;

        if ( Float.compare( predmet.vaha, vaha ) != 0 ) return false;
        if ( nositelne != predmet.nositelne ) return false;
        return nazev.equals( predmet.nazev );
    }

    @Override
    public int hashCode() {
        int result = nazev.hashCode();
        result = 31 * result + ( vaha != +0.0f ? Float.floatToIntBits( vaha ) : 0 );
        result = 31 * result + ( nositelne ? 1 : 0 );
        return result;
    }

    public String getNazev() {
        return nazev;
    }

    public float getVaha() {
        return vaha;
    }

    public boolean isNositelne() {
        return nositelne;
    }
}
