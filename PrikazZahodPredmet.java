/**
 * Zahozeni predmetu z batohu
 * Created by matejcermak on 14.03.18.
 */
public class PrikazZahodPredmet implements IPrikaz {


    private static final String NAZEV = "zahod";
    private HerniPlan plan;

    public PrikazZahodPredmet( HerniPlan plan ) {
        this.plan = plan;
    }

    /**
     * Predmet je vyhozen z batohu do mistnosti, kde se hrac aktualne nachazi a je znovu sebratelna
     *
     * @param parametry počet parametrů závisí na konkrétním příkazu.
     */
    @Override
    public String proved( String... parametry ) {
        if ( parametry.length == 0 ) {
            return "Potreba zadat nazev predmetu!";
        }

        String nazevVeci = parametry[ 0 ];


        Predmet predmet = this.plan.getBatoh().getPredmetByName( nazevVeci );

        if ( predmet == null )
            return "Predmet neni v batohu!";

        this.plan.getAktualniProstor().pridatPredmet( predmet );
        this.plan.getBatoh().odebratVec( predmet );
        return "Predmet vyhozen z batohu.";
    }

    @Override
    public String getNazev() {
        return NAZEV;
    }
}
