/**
 * Umoznuje vzit predmet umisteny v mistnosti
 * Created by matejcermak on 14.03.18.
 */
public class PrikazVezmiPredmet implements IPrikaz {

    private static final String NAZEV = "vezmi";
    private HerniPlan plan;

    public PrikazVezmiPredmet( HerniPlan plan ) {
        this.plan = plan;
    }

    /**
     * metoda kontroluje kapacitu batohu a nositelnost predmetu
     * pokud vse vyhovuje, predmet je pridan do batohu
     *
     * @param parametry počet parametrů závisí na konkrétním příkazu.
     */
    @Override
    public String proved( String... parametry ) {
        if ( parametry.length == 0 ) {
            return "Potreba zadat nazev predmetu!";
        }

        String jmenoPredmetu = parametry[ 0 ];
        Predmet predmet = this.plan.getAktualniProstor().getPredmetByName( jmenoPredmetu );

        if ( predmet == null )
            return "Predmet neni v mistnosti!";
        if ( !predmet.isNositelne() )
            return "Predmet neni nositelny!";
        if ( !this.plan.getBatoh().pridatVec( predmet ) )
            return "Neni kapacita v batohu!";

        this.plan.getAktualniProstor().getPredmety().remove( predmet );
        return "Predmet pridan do batohu.";
    }

    @Override
    public String getNazev() {
        return NAZEV;
    }
}
