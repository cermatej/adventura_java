/**
 * Zakladni postava bez zadne dalsi funkcionality, vetsinou slouzi k pouziti predmetu
 * Created by matejcermak on 11.04.18.
 */
public class ZakladniPostava extends Postava {

    public ZakladniPostava( String jmeno ) {
        super( jmeno );
    }
}
