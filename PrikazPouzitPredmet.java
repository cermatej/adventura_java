/**
 * Umoznuje pouzit predmet na urcitou postavu
 * Created by matejcermak on 11.04.18.
 */
public class PrikazPouzitPredmet implements IPrikaz {

    private static final String NAZEV = "pouzij";
    private HerniPlan plan;

    public PrikazPouzitPredmet( HerniPlan plan ) {
        this.plan = plan;
    }

    /**
     * Pouziti predmetu - pokud predmet odpovida pouziti vychody Prostoru jsou otevreny
     *
     * @param parametry počet parametrů závisí na konkrétním příkazu.
     */
    @Override
    public String proved( String... parametry ) {

        if ( parametry.length != 2 ) {
            return "Potreba zadat nazev predmetu a postavu, na kterou pouzit!";
        }

        Predmet predmet = this.plan.getBatoh().getPredmetByName( parametry[ 0 ] );
        Postava postava = this.plan.getAktualniProstor().getPostavaByName( parametry[ 1 ] );

        if ( predmet == null || postava == null )
            return "Postava nebo predmet neexistuje!";

        if ( !predmet.pouzit( postava ) ) {
            return "Predmet nelze takto pouzit!";
        }

        this.plan.getAktualniProstor().setVychodVolny( true );
        return "Predmet pouzit spravne. Mistnost volna!";
    }

    @Override
    public String getNazev() {
        return NAZEV;
    }
}
