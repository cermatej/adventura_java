import java.util.HashSet;
import java.util.Set;

/**
 * Created by matejcermak on 14.03.18.
 * Trida reprezentujici hracuv batoh ve hre, ma omezenou nosnost, hrac muze predmety pridavat i vyhazovat
 */
public class Batoh {

    private Set<Predmet> seznamPredmetu;
    private float nosnost;

    /**
     * @param nosnost maximalni nosnost batohu, nelze prekrocit pri pridavani predmetu
     */
    public Batoh( float nosnost ) {
        this.nosnost = nosnost;
        this.seznamPredmetu = new HashSet<>();
    }

    public boolean odebratVec( Predmet p ) {
        return this.seznamPredmetu.remove( p );
    }

    public boolean pridatVec( Predmet p ) {
        if ( ( this.getAktualniVaha() + p.getVaha() ) > this.nosnost )
            return false;
        this.seznamPredmetu.add( p );
        return true;
    }

    /**
     * @return aktualni vaha batohu pocitana v case volani pruchodem pres predmety v nem
     */
    public float getAktualniVaha() {
        float aktualniVaha = 0;
        for ( Predmet predmet :
                this.seznamPredmetu ) {
            aktualniVaha += predmet.getVaha();
        }
        return aktualniVaha;
    }

    public Set<Predmet> getSeznamPredmetu() {
        return seznamPredmetu;
    }

    public Predmet getPredmetByName( String name ) {
        for ( Predmet predmet : this.seznamPredmetu ) {
            if ( predmet.getNazev().equals( name ) ) {
                return predmet;
            }
        }
        return null;
    }
}

