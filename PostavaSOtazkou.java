/**
 * Odvozena postava. Lze s ni interagovat (otazka na kterou je prave 1 spravna odpoved)
 * Created by matejcermak on 11.04.18.
 */
public class PostavaSOtazkou extends Postava {

    private String otazka;
    private int spravnaOdpoved;

    /**
     * @param jmeno          jmeno postavy
     * @param otazka         otazka, kterou polozi po interakci s ni
     * @param spravnaOdpoved poradi spravne odpovedi na polozenou otazku
     */
    public PostavaSOtazkou( String jmeno, String otazka, int spravnaOdpoved ) {
        super( jmeno );
        this.otazka = otazka;
        this.spravnaOdpoved = spravnaOdpoved;
    }

    public String getOtazka() {
        return otazka;
    }

    public int getSpravnaOdpoved() {
        return spravnaOdpoved;
    }
}
