import java.util.Set;

/**
 * Zobrazi obsah hracova batohu a jeho vaha
 * Created by matejcermak on 14.03.18.
 */
public class PrikazZobrazInventar implements IPrikaz {

    private static final String NAZEV = "inventar";
    private HerniPlan plan;


    public PrikazZobrazInventar( HerniPlan plan ) {
        this.plan = plan;
    }

    @Override
    public String proved( String... parametry ) {
        String inv = "";
        float vaha = this.plan.getBatoh().getAktualniVaha();
        Set<Predmet> predmety = this.plan.getBatoh().getSeznamPredmetu();

        inv += "Vaha: " + vaha + "kg\n";

        for ( Predmet p :
                predmety ) {
            inv += p.getNazev() + " (" + p.getVaha() + "kg)\n";
        }

        return inv;
    }

    @Override
    public String getNazev() {
        return NAZEV;
    }
}
