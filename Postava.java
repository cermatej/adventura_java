/**
 * Abstraktni trida postava, umistitelna do mistnosti, v odvozenych tridach implementovane ruzne chovani postav.
 * Created by matejcermak on 14.03.18.
 */
public abstract class Postava {

    private String jmeno;

    public Postava( String jmeno ) {
        this.jmeno = jmeno;
    }

    public String getJmeno() {
        return jmeno;
    }

}
