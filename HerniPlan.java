
/**
 * Class HerniPlan - třída představující mapu a stav adventury.
 * <p>
 * Tato třída inicializuje prvky ze kterých se hra skládá:
 * vytváří všechny prostory,
 * propojuje je vzájemně pomocí východů
 * a pamatuje si aktuální prostor, ve kterém se hráč právě nachází.
 *
 * @author Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova
 * @version pro školní rok 2013/2014
 */
public class HerniPlan {

    private static final float DEFAULT_VAHA_BATOHU = 10;

    private Prostor aktualniProstor;
    private Batoh batoh;

    /**
     * Konstruktor který vytváří jednotlivé prostory a propojuje je pomocí východů.
     * Jako výchozí aktuální prostor nastaví temnyLes
     */
    public HerniPlan() {
        zalozProstoryHry();
        this.batoh = new Batoh( HerniPlan.DEFAULT_VAHA_BATOHU );
    }

    /**
     * Vytváří jednotlivé prostory a propojuje je pomocí východů.
     * Jako výchozí aktuální prostor nastaví domeček.
     */
    private void zalozProstoryHry() {
        // prostory
        Prostor temnyLes = new Prostor( "les", "Probudil ses v tajemnem lese, kousek v mechu muzes videt nuz", true );
        Prostor starcuvMost = new Prostor( "most", "Most pres reku smrti, pred nim stojici starec", false );
        Prostor vlkJeskyne = new Prostor( "jeskyne", "Temna jeskyne, je potreba projit pres ni. V ceste ti ale stoji vlkodlak. U nohou ti lezi jablko", false );
        Prostor bazina = new Prostor( "bazina", "Bazina s hladovym zebrakem. Dej mu trochu najist a pomuze ti pres bazinu", false );
        Prostor truhlaMistnost = new Prostor( "chram", "Chram s truhlami. Uz jen posledni krok! Zeptej se vojaka", false );

        // postavy
        Postava vlkodlak = new ZakladniPostava( "vlkodlak" );
        Postava starec = new PostavaSOtazkou( "starec", "Odpověď na základní otázku života, vesmíru a vůbec \n 1) 42 \n 2) 3.14159265359 \n 3) buh ", 1 );
        Postava zebrak = new ZakladniPostava( "zebrak" );
        Postava vojak = new PostavaSOtazkou( "vojak", "Ve ktere truhle je svaty gral? \n 1) 1.truhla \n 2) 2.truhla \n 3) 3.truhla ", 2 );

        starcuvMost.pridatPostavu( starec );
        vlkJeskyne.pridatPostavu( vlkodlak );
        bazina.pridatPostavu( zebrak );
        truhlaMistnost.pridatPostavu( vojak );

        // predmety
        temnyLes.pridatPredmet( new Predmet( "nuz", 7, true ) {
            @Override
            public boolean pouzit( Postava postava ) {
                return postava.equals( vlkodlak );
            }
        } );
        vlkJeskyne.pridatPredmet( new Predmet( "jablko", 4, true ) {
            @Override
            public boolean pouzit( Postava postava ) {
                return postava.equals( zebrak );
            }
        } );
        bazina.pridatPredmet( new Predmet( "uchostour", 150, false ) {
            @Override
            public boolean pouzit( Postava postava ) {
                return true;
            }
        } );

        // přiřazují se průchody mezi prostory (sousedící prostory)
        temnyLes.setVychod( starcuvMost );
        starcuvMost.setVychod( vlkJeskyne );
        vlkJeskyne.setVychod( bazina );
        bazina.setVychod( truhlaMistnost );

        this.setAktualniProstor( temnyLes );
    }

    /**
     * Metoda vrací odkaz na aktuální prostor, ve kterém se hráč právě nachází.
     *
     * @return aktuální prostor
     */

    public Prostor getAktualniProstor() {
        return aktualniProstor;
    }

    /**
     * Metoda nastaví aktuální prostor, používá se nejčastěji při přechodu mezi prostory
     *
     * @param prostor nový aktuální prostor
     */
    public void setAktualniProstor( Prostor prostor ) {
        aktualniProstor = prostor;
    }

    public Batoh getBatoh() {
        return batoh;
    }

}
