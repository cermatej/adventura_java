import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída SeznamPrikazuTest slouží ke komplexnímu otestování třídy  
 * SeznamPrikazu
 *
 * @author Luboš Pavlíček
 * @version pro školní rok 2013/2014
 */
public class SeznamPrikazuTest {
    private Hra hra;
    private PrikazKonec prKonec;
    private PrikazJdi prJdi;
    private PrikazNapoveda prNapo;
    private PrikazZobrazInventar prInv;
    private PrikazVezmiPredmet prVezmi;
    private PrikazZahodPredmet prZahod;
    private PrikazInteragovat prInter;
    private PrikazPouzitPredmet prPouzij;

    @Before
    public void setUp() {
        hra = new Hra();
        prKonec = new PrikazKonec( hra );
        prJdi = new PrikazJdi( hra.getHerniPlan() );
        prNapo = new PrikazNapoveda( hra.getPlatnePrikazy() );
        //nove prikazy
        prInv = new PrikazZobrazInventar( hra.getHerniPlan() );
        prVezmi = new PrikazVezmiPredmet( hra.getHerniPlan() );
        prZahod = new PrikazZahodPredmet( hra.getHerniPlan() );
        prInter = new PrikazInteragovat( hra.getHerniPlan(), hra );
        prPouzij = new PrikazPouzitPredmet( hra.getHerniPlan() );
    }

    /**
     * Vlozeni a vybrani prikazu ze seznamu dostupnych prikazu
     */
    @Test
    public void testVlozeniVybrani() {
        SeznamPrikazu seznPrikazu = new SeznamPrikazu();
        seznPrikazu.vlozPrikaz( prKonec );
        seznPrikazu.vlozPrikaz( prJdi );
        assertEquals( prKonec, seznPrikazu.vratPrikaz( "konec" ) );
        assertEquals( prJdi, seznPrikazu.vratPrikaz( "jdi" ) );
        assertEquals( null, seznPrikazu.vratPrikaz( "napoveda" ) );
    }

    /**
     * Kontrola spravne syntaxe volani prikazu
     */
    @Test
    public void testJePlatnyPrikaz() {
        SeznamPrikazu seznPrikazu = new SeznamPrikazu();
        pridatPrikazy( seznPrikazu );

        assertEquals( true, seznPrikazu.jePlatnyPrikaz( "konec" ) );
        assertEquals( true, seznPrikazu.jePlatnyPrikaz( "jdi" ) );
        assertEquals( true, seznPrikazu.jePlatnyPrikaz( "napoveda" ) );
        assertEquals( true, seznPrikazu.jePlatnyPrikaz( "inventar" ) );
        assertEquals( true, seznPrikazu.jePlatnyPrikaz( "vezmi" ) );
        assertEquals( true, seznPrikazu.jePlatnyPrikaz( "zahod" ) );
        assertEquals( true, seznPrikazu.jePlatnyPrikaz( "interagovat" ) );
        assertEquals( true, seznPrikazu.jePlatnyPrikaz( "pouzij" ) );

        assertEquals( false, seznPrikazu.jePlatnyPrikaz( "Konec" ) );
        assertEquals( false, seznPrikazu.jePlatnyPrikaz( "Kolskdjflasdkc" ) );
        assertEquals( false, seznPrikazu.jePlatnyPrikaz( "Napoveda" ) );
    }

    private void pridatPrikazy( SeznamPrikazu seznPrikazu ) {
        seznPrikazu.vlozPrikaz( prKonec );
        seznPrikazu.vlozPrikaz( prJdi );
        seznPrikazu.vlozPrikaz( prNapo );
        seznPrikazu.vlozPrikaz( prInv );
        seznPrikazu.vlozPrikaz( prVezmi );
        seznPrikazu.vlozPrikaz( prZahod );
        seznPrikazu.vlozPrikaz( prInter );
        seznPrikazu.vlozPrikaz( prPouzij );
    }

    /**
     * Otestovani nazvu prikazu
     */
    @Test
    public void testNazvyPrikazu() {
        SeznamPrikazu seznPrikazu = new SeznamPrikazu();
        seznPrikazu.vlozPrikaz( prKonec );
        seznPrikazu.vlozPrikaz( prJdi );
        String nazvy = seznPrikazu.vratNazvyPrikazu();
        assertEquals( true, nazvy.contains( "konec" ) );
        assertEquals( true, nazvy.contains( "jdi" ) );
        assertEquals( false, nazvy.contains( "napoveda" ) );
        assertEquals( false, nazvy.contains( "Konec" ) );
    }

}
