import java.util.Scanner;

/**
 * Umoznuje interagovat s postavami tridy PostavaSOtazkou
 * Created by matejcermak on 14.03.18.
 */
public class PrikazInteragovat implements IPrikaz {
    private static final String NAZEV = "interagovat";
    private HerniPlan plan;
    private Hra hra;

    public PrikazInteragovat( HerniPlan plan, Hra hra ) {
        this.plan = plan;
        this.hra = hra;
    }

    /**
     * samotna interakce, pokud hrac odpovi spatne, hra konci
     * pri spravne odpovedi ma otevrene vychody mistnosti
     *
     * @param parametry počet parametrů závisí na konkrétním příkazu.
     */
    @Override
    public String proved( String... parametry ) {
        if ( parametry.length == 0 ) {
            return "Potreba zadat jmeno postavy!";
        }

        String jmenoPostavy = parametry[ 0 ];
        Postava postava = this.plan.getAktualniProstor().getPostavaByName( jmenoPostavy );

        if ( postava == null )
            return "Postava neni v mistnosti!";

        if ( !( postava instanceof PostavaSOtazkou ) )
            return "S postavou nelze interagovat!";

        PostavaSOtazkou p = (PostavaSOtazkou) postava;

        System.out.println( p.getOtazka() );
        int odpoved = this.prectiCisloOdpovedi();

        if ( p.getSpravnaOdpoved() != odpoved ) {
            this.hra.setKonecHry( true );
            return "Spatna odpoved - konec hry!";
        }

        if ( !this.plan.getAktualniProstor().getVychody().isEmpty() ) {
            this.plan.getAktualniProstor().setVychodVolny( true );
            return "Spravna odpoved- muzes pokracovat!";
        }

        // hra vyhrana, nejsou dalsi vychody
        this.hra.setKonecHry( true );
        return "Ziskal jsi zlaty gral, vyhral jsi hru!";
    }

    private int prectiCisloOdpovedi() {
        Scanner scanner = new Scanner( System.in );
        System.out.print( "> " );
        return Integer.parseInt( scanner.nextLine() );
    }


    @Override
    public String getNazev() {
        return NAZEV;
    }
}
