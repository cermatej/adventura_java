
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída ProstorTest slouží ke komplexnímu otestování
 * třídy Prostor
 *
 * @author Jarmila Pavlíčková
 * @version pro skolní rok 2013/2014
 */
public class ProstorTest {
    //== Datové atributy (statické i instancí)======================================

    //== Konstruktory a tovární metody =============================================
    //-- Testovací třída vystačí s prázdným implicitním konstruktorem ----------

    //== Příprava a úklid přípravku ================================================

    /***************************************************************************
     * Metoda se provede před spuštěním každé testovací metody. Používá se
     * k vytvoření tzv. přípravku (fixture), což jsou datové atributy (objekty),
     * s nimiž budou testovací metody pracovat.
     */
    @Before
    public void setUp() {
    }

    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každé testovací metody.
     */
    @After
    public void tearDown() {
    }

    //== Soukromé metody používané v testovacích metodách ==========================

    //== Vlastní testovací metody ==================================================

    /**
     * Testuje, zda jsou správně nastaveny průchody mezi prostory hry. Prostory
     * nemusí odpovídat vlastní hře,
     */
    @Test
    public void testLzeProjit() {
        Prostor temnyLes = new Prostor( "les", "Probudil ses v tajemnem lese", true );
        Prostor starcuvMost = new Prostor( "most", "Most pres reku smrti, pred nim stoji starec", false );

        temnyLes.setVychod( starcuvMost );
        assertEquals( starcuvMost, temnyLes.vratSousedniProstor( "most" ) );
        assertEquals( null, starcuvMost.vratSousedniProstor( "neexistujici" ) );
    }

}
