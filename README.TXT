# Svaty gral 0.1.0 - Matej Cermak
### Popis
Jedná se o jednoduchou hru typu advetura s textovým uživatelským rozhraním.
Hra zatím obsahuje jednoduchý herní plán s pěti místnostmi. Hráč může přecházet z jedné místnosti do druhé.
Mistnosti obsahuji predmety, jen nektere z nich jsou ale sebratelne.
V mistnostech jsou umistene postavy, s nekterymi z nich lze interagovat, na nektere lze pouzit predmet.
Po uspesne konfrontaci s postavou (spravne pouziti predmetu, spravna odpoved na otazku) je uvolnen vstup do dalsi mistnosti.
## Spusteni
Hra se spouští v BlueJ tak, že vytvoříte instanci třídy TextoveRozhrani a zavoláte metodu hraj() nebo spustenim main metody v tride Adventura

Další podrobnosti k adventuře lze najít na java.vse.cz
