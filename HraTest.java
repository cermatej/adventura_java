import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;

import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída HraTest slouží ke komplexnímu otestování
 * třídy Hra
 *
 * @author Jarmila Pavlíčková
 * @version pro školní rok 2013/2014
 */
public class HraTest {
    private Hra hra1;

    //== Datové atributy (statické i instancí)======================================

    //== Konstruktory a tovární metody =============================================
    //-- Testovací třída vystačí s prázdným implicitním konstruktorem ----------

    //== Příprava a úklid přípravku ================================================

    /***************************************************************************
     * Metoda se provede před spuštěním každé testovací metody. Používá se
     * k vytvoření tzv. přípravku (fixture), což jsou datové atributy (objekty),
     * s nimiž budou testovací metody pracovat.
     */
    @Before
    public void setUp() {
        hra1 = new Hra();
    }

    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každé testovací metody.
     */
    @After
    public void tearDown() {
    }

    //== Soukromé metody používané v testovacích metodách ==========================

    //== Vlastní testovací metody ==================================================

    /***************************************************************************
     * Testuje průběh hry, po zavolání každěho příkazu testuje, zda hra končí
     * a v jaké aktuální místnosti se hráč nachází.
     * Při dalším rozšiřování hry doporučujeme testovat i jaké věci nebo osoby
     * jsou v místnosti a jaké věci jsou v batohu hráče.
     *
     */
    @Test
    public void testPrubehHry() {
        // lese
        assertEquals( "les", hra1.getHerniPlan().getAktualniProstor().getNazev() );
        testVezmiZahod();
        hra1.zpracujPrikaz( "jdi most" );

        // most
        assertEquals( "most", hra1.getHerniPlan().getAktualniProstor().getNazev() );
        assertEquals( false, hra1.konecHry() ); // hra nekonci
        testVychodNeniVolny();
        assertEquals( "Predmet nelze takto pouzit!", hra1.zpracujPrikaz( "pouzij nuz starec" ) ); // nelze pouzit predmet

        System.setIn( new ByteArrayInputStream( "1".getBytes() ) );
        hra1.zpracujPrikaz( "interagovat starec" );
        hra1.zpracujPrikaz( "jdi jeskyne" );

        // jeskyne
        assertEquals( "jeskyne", hra1.getHerniPlan().getAktualniProstor().getNazev() );
        assertEquals( false, hra1.konecHry() ); // hra nekonci
        testPlnyBatoh();
        assertEquals( "Vychod neni volny!", hra1.zpracujPrikaz( "jdi bazina" ) ); // nelze jit
        hra1.zpracujPrikaz( "pouzij nuz vlkodlak" ); // zabit vlkodlaka
        hra1.zpracujPrikaz( "zahod nuz" );
        hra1.zpracujPrikaz( "vezmi jablko" );
        hra1.zpracujPrikaz( "jdi bazina" );

        // bazina
        assertEquals( "bazina", hra1.getHerniPlan().getAktualniProstor().getNazev() );
        assertEquals( false, hra1.konecHry() ); // hra nekonci
        testNeniNositelny();
        pouzijSpatnaSyntaxe();
        hra1.zpracujPrikaz( "pouzij jablko zebrak" );
        hra1.zpracujPrikaz( "jdi chram" );

        // chram
        assertEquals( "chram", hra1.getHerniPlan().getAktualniProstor().getNazev() );
        assertEquals( false, hra1.konecHry() ); // hra nekonci
        System.setIn( new ByteArrayInputStream( "2".getBytes() ) );
        hra1.zpracujPrikaz( "interagovat vojak" );

        assertEquals( true, hra1.konecHry() ); // konec hry
    }

    /**
     * Testovani, zda lze vzit predmet, ktery neni nositelny
     */
    private void testNeniNositelny() {
        assertEquals( "Predmet neni nositelny!", hra1.zpracujPrikaz( "vezmi uchostour" ) ); // nesebratelna vec
    }

    /**
     * Puziti spatne syntaxe prikazu pouzij
     */
    private void pouzijSpatnaSyntaxe() {
        assertEquals( "Postava nebo predmet neexistuje!", hra1.zpracujPrikaz( "pouzij lkasdkjf zebrak" ) ); // neexistujici predmet
        assertEquals( "Postava nebo predmet neexistuje!", hra1.zpracujPrikaz( "pouzij jablko ljaksdfl" ) ); // neexistujici postava
        assertEquals( "Potreba zadat nazev predmetu a postavu, na kterou pouzit!", hra1.zpracujPrikaz( "pouzij jablko" ) ); // nespravne pouziti fce
        assertEquals( "Potreba zadat nazev predmetu a postavu, na kterou pouzit!", hra1.zpracujPrikaz( "pouzij" ) ); // nespravne pouziti fce
    }

    /**
     * Neni mozne nabrat predmety pres nosnost batohu
     */
    private void testPlnyBatoh() {
        assertEquals( "Neni kapacita v batohu!", hra1.zpracujPrikaz( "vezmi jablko" ) ); // neni kapacita v batohu
    }

    /**
     * Nelze odejit zavrenym vychodem
     */
    private void testVychodNeniVolny() {
        assertEquals( "Vychod neni volny!", hra1.zpracujPrikaz( "jdi jeskyne" ) ); // nelze jit
    }

    /**
     * Test zahozeni a sebrani predmetu s kontrolou, zda je vyhozeny predmet dostupny v mistnosti
     */
    private void testVezmiZahod() {
        assertNotNull( hra1.getHerniPlan().getAktualniProstor().getPredmetByName( "nuz" ) ); // je v prostoru
        hra1.zpracujPrikaz( "vezmi nuz" );
        assertNotNull( hra1.getHerniPlan().getBatoh().getPredmetByName( "nuz" ) ); // je v batohu
        assertNull( hra1.getHerniPlan().getAktualniProstor().getPredmetByName( "nuz" ) ); // neni v prostoru
        hra1.zpracujPrikaz( "zahod nuz" );
        assertNull( hra1.getHerniPlan().getBatoh().getPredmetByName( "nuz" ) ); // neni v batohu
        assertNotNull( hra1.getHerniPlan().getAktualniProstor().getPredmetByName( "nuz" ) ); // je v prostoru
        hra1.zpracujPrikaz( "vezmi nuz" );
    }

    /**
     * Zda lze ukoncit hru
     */
    @Test
    public void prikazKonecTest() {
        hra1.zpracujPrikaz( "konec" );
        assertEquals( true, hra1.konecHry() );
    }

}
